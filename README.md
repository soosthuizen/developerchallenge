# README #
## Developer Challenge ##

[Please follow this link to access the deployed application...](https://developerchallenge-bqkpw757.launchpad.cfapps.ap21.hana.ondemand.com/b2b96ca0-ed73-4ec5-85ff-75862c9dfc2d.au-com-bournedigital-developerchallenge.aucombournedigitaldeveloperchallenge-0.0.1/index.html)

![worklist](/img/worklist-1.png)

![quickview](/img/quickview-1.png)

![object](/img/object-1.png)

## Highligths ##

### Orders Screen ###
- Search by Customer Name
    - This includes Customer ID and Company Name
- Search by Employee Responsible
    - This includes by First Name, Last Name or both combined
- Search by Order ID
- Order Number shows without decimal places
- Date is in the right format
- Employee Responsible is displayed as a link to enable Quick View

### Quick View ###
- Quick View opens to the right of the Employee Name
- Icon is turquoise with no image

### Order Items Screen ###
- Navigation to Order Items screen fixed
- Back button enabled for navigating back to Orders screen
- Order Id appears next to the Customer Name and is correctly alligned
- Address displays the correct value
    - Had the Contact Name before
- Total Value improvements    
    - Total value calculation fixed
        - Unit Price x Quantity - Calculated Discount
    - Rounded to 2 decimal places
    - The total amount is displayed in a larger font to stand out
- Unit price is displayed with 2 decimal places
- Discount appears in a different colour