sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
], function (BaseController, JSONModel, formatter, Filter, FilterOperator, MessageToast, Fragment, Controller) {
    "use strict";

    return BaseController.extend("au.com.bournedigital.developerchallenge.controller.Worklist", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        /**
         * Called when the worklist controller is instantiated.
         * @public
         */
        onInit : function () {
            var oViewModel;

            // keeps the search state
            this._aTableSearchState = [];

            // Model used to manipulate control states
            oViewModel = new JSONModel({
                worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
                shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
                shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
                tableNoDataText : this.getResourceBundle().getText("tableNoDataText")
            });
            this.setModel(oViewModel, "worklistView");

            var employeeData = new JSONModel(sap.ui.require.toUrl("au/com/bournedigital/developerchallenge/model/employee.json"));
            this.getView().setModel(employeeData, "EmployeeModel");

        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Triggered by the table's 'updateFinished' event: after new table
         * data is available, this handler method updates the table counter.
         * This should only happen if the update was successful, which is
         * why this handler is attached to 'updateFinished' and not to the
         * table's list binding's 'dataReceived' method.
         * @param {sap.ui.base.Event} oEvent the update finished event
         * @public
         */
        onUpdateFinished : function (oEvent) {
            // update the worklist's object counter after the table update
            var sTitle,
                oTable = oEvent.getSource(),
                iTotalItems = oEvent.getParameter("total");
            // only update the counter if the length is final and
            // the table is not empty
            if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
                sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
            } else {
                sTitle = this.getResourceBundle().getText("worklistTableTitle");
            }
            this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
        },

        /**
         * Event handler when a table item gets pressed
         * @param {sap.ui.base.Event} oEvent the table selectionChange event
         * @public
         */
        onPress : function (oEvent) {
            // The source is the list item that got pressed
            this._showObject(oEvent.getSource());
        },

        /**
         * Event handler for navigating back.
         * Navigate back in the browser history
         * @public
         */
        onNavBack : function() {
            // eslint-disable-next-line sap-no-history-manipulation
            history.go(-1);
        },


        onSearch : function (oEvent) {
            if (oEvent.getParameters().refreshButtonPressed) {
                // Search field's 'refresh' button has been pressed.
                // This is visible if you select any main list item.
                // In this case no new search is triggered, we only
                // refresh the list binding.
                this.onRefresh();
            } else {
                var aTableSearchState = [];
                var sQuery = oEvent.getParameter("query");

                if (sQuery && sQuery.length > 0) {

                    // Split Employee First Name and Last Name so that the user is able
                    // to search each of them individually or combined
                    var aString = sQuery.split(" ");
                    if(aString.length > 1) {
                        var sQueryPart1 = aString[0];
                        var sQueryPart2 = aString[1];
                    }

                    // Init Filter
                    var aFilters = [];

                    // Enable user to search by Order ID
                    if(!isNaN(sQuery)) {
                        aFilters.push(new Filter("OrderID", FilterOperator.EQ, parseInt(sQuery)));
                    }

                    // Enable user to search by Customer ID or Company Name
                    aFilters.push(new Filter("CustomerID", FilterOperator.Contains, sQuery));
                    aFilters.push(new Filter("Customer/CompanyName", FilterOperator.Contains, sQuery),);
                    
                    // Enable user to search by Employee First Name
                    aFilters.push(new Filter("Employee/FirstName", FilterOperator.Contains, sQuery));
                    if(sQueryPart1) {
                        aFilters.push(new Filter("Employee/FirstName", FilterOperator.Contains, sQueryPart1));
                    }
                    if(sQueryPart2) {
                        aFilters.push(new Filter("Employee/FirstName", FilterOperator.Contains, sQueryPart2));
                    }

                    // Enable user to search by Employa Last Name
                    aFilters.push(new Filter("Employee/LastName", FilterOperator.Contains, sQuery));
                    if(sQueryPart1) {
                        aFilters.push(new Filter("Employee/LastName", FilterOperator.Contains, sQueryPart1));
                    }
                    if(sQueryPart2) {
                        aFilters.push(new Filter("Employee/LastName", FilterOperator.Contains, sQueryPart2));
                    }

                    aFilters.push();
                    aTableSearchState = [new Filter({
                        filters: aFilters,
                        and: false
                    })];
                }
                this._applySearch(aTableSearchState);
            }
        },

        /**
         * Event handler for refresh event. Keeps filter, sort
         * and group settings and refreshes the list binding.
         * @public
         */
        onRefresh : function () {
            var oTable = this.byId("table");
            oTable.getBinding("items").refresh();
        },

        /* =========================================================== */
        /* internal methods                                            */
        /* =========================================================== */

        /**
         * Shows the selected item on the object page
         * @param {sap.m.ObjectListItem} oItem selected Item
         * @private
         */
        _showObject : function (oItem) {
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext().getPath().substring("/Orders".length)
            });
        },

        /**
         * Internal helper method to apply both filter and search state together on the list binding
         * @param {sap.ui.model.Filter[]} aTableSearchState An array of filters for the search
         * @private
         */
        _applySearch: function(aTableSearchState) {
            var oTable = this.byId("table"),
                oViewModel = this.getModel("worklistView");
            oTable.getBinding("items").filter(aTableSearchState, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aTableSearchState.length !== 0) {
                oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
            }
        },

        onEmployeePress: function(oEvent) {
            var order = oEvent.getSource().getBindingContext().getObject();
            
            // Retrieve context and update Employee Data
            this._updateEmployeeData(this.getModel().oData[order.Employee.__ref]);
            
            var oModel = this.getView().getModel("EmployeeModel"),
                oSource = oEvent.getSource(),
			    oView = this.getView();

			// Create Quick View
			if (!this._pQuickView) {
				this._pQuickView = Fragment.load({
					id: oView.getId(),
					name: "au.com.bournedigital.developerchallenge.view.fragment.EmployeeQuickView",
					controller: this
				}).then(function(oQuickView) {
                    oQuickView.setModel(oModel);
					oView.addDependent(oQuickView);
					return oQuickView;
				});
			}

			this._pQuickView.then(function(oQuickView) {
                oQuickView.openBy(oSource);
			});
        },

        _updateEmployeeData: function(employee) {
            var oResourceBundle = this.getModel("i18n").getResourceBundle();

            // Get Employee Data
            var employeeModel = this.getView().getModel("EmployeeModel");
            
            // Update Employee Data 
            if(employeeModel.getData().pages.length > 0) {
                var employeeData = employeeModel.getData();

                // Get index of Employee Detail Page
                var employeeDetailPageIndex = employeeData.pages.map(function(e) { return e.pageId; }).indexOf("idQuickViewEmployeePage");
                
                // Get index of Address Detail Group
                var addressDetailGroupIndex = employeeData.pages[employeeDetailPageIndex].groups.map(function(e) { return e.id; }).indexOf("idQuickViewAddressDetails");
                
                // Get indexes of Address, City, Post Code and Phone
                var addressIndex = employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements.map(function(e) { return e.id; }).indexOf("idQuickViewAddress");
                var cityIndex = employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements.map(function(e) { return e.id; }).indexOf("idQuickViewCity");
                var postCodeIndex = employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements.map(function(e) { return e.id; }).indexOf("idQuickViewPostCode");
                var phoneIndex = employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements.map(function(e) { return e.id; }).indexOf("idQuickViewPhone");

                // Set Employee Data Values
                employeeData.pages[employeeDetailPageIndex].header = oResourceBundle.getText("employeeDetail");
                employeeData.pages[employeeDetailPageIndex].title = `${employee.FirstName} ${employee.LastName}`;
                employeeData.pages[employeeDetailPageIndex].description = employee.Title;
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].heading = oResourceBundle.getText("addressDetails");
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[addressIndex].label = oResourceBundle.getText("empAddress");
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[addressIndex].value = employee.Address;
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[cityIndex].label = oResourceBundle.getText("empCity");
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[cityIndex].value = employee.City;
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[postCodeIndex].label = oResourceBundle.getText("empPostCode");
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[postCodeIndex].value = employee.PostalCode;
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[phoneIndex].label = oResourceBundle.getText("empPhone");
                employeeData.pages[employeeDetailPageIndex].groups[addressDetailGroupIndex].elements[phoneIndex].value = employee.HomePhone;
            
                // Update Employee Model
                employeeModel.setData(employeeData);
            }
        },

    });
});
